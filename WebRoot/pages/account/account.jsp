<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<style>
		.input-text {width: 400px;}
	</style>
</head>

<body>
	<div class="wrapper">
	
		<div class="tabbable tabs-left">
			<ul class="nav nav-tabs">
				<li class="active" type="1"><a href="#tab1" data-toggle="tab">项目经理</a></li>
				<li type="2"><a href="#tab2" data-toggle="tab">研发工程师</a></li>
				<li type="3"><a href="#tab3" data-toggle="tab">测试工程师</a></li>
			</ul>
			
			<div class="tab-content">
				
				<div id="btnDiv" class="hide">
					<button onclick="createUser();" class="btn">添加用户</button>
				</div>
				
				<div id="listDiv">
					<table class="list-table">
						<tr>
							<th width="60px">序号</th>
							<th width="150px">用户名</th>
							<th width="200px">Email</th>
							<th width="400px">地址</th>
							<th width="50px"></th>
						</tr>
					</table>
				</div>
			
				<div class="tab-pane active" id="tab1"></div>
				<div class="tab-pane" id="tab2"></div>
				<div class="tab-pane" id="tab3"></div>
			</div>
		</div>
		
	</div>
	
	<!-- Modal -->
	<div id="userModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3 id="myModalLabel"></h3>
		</div>
		
		<div class="modal-body">
			<table style="font-size: 13px;">
				<tr>
					<td align="right" style="padding-right: 15px;">用户名</td>
					<td><input id="username" type="text" class="input-text input-require" /></td>
				</tr>
				<tr>
					<td align="right" style="padding-right: 15px;">密码</td>
					<td><input id="password" type="password" class="input-text input-require" /></td>
				</tr>
				<tr>
					<td align="right" style="padding-right: 15px;">Email</td>
					<td><input id="email" type="text" class="input-text input-require" /></td>
				</tr>
				<tr>
					<td align="right" style="padding-right: 15px;">地址</td>
					<td><input id="address" type="text" class="input-text" /></td>
				</tr>
			</table>
		</div>
		
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">关闭</button>
			<button onclick="saveUser();" class="btn btn-primary">保存</button>
		</div>
	</div>
	
	<div id="dialog-confirm" class="hide" title="缺陷删除">
		<p style="padding: 5px;"><i class="icon-trash" style="margin-right: 15px;"></i></span>确定要从缺陷库中删除该缺陷么？</p>
	</div>
	
	<div id="dialog-del" class="hide" title="删除用户">
		<p>删除用户成功！</p>
	</div>
	
	<script>
		var globalUserId, isAdmin;

		_remoteCall("user/isAdmin.do", null, function(data) { isAdmin = data == "Y";  if(isAdmin)  $("#btnDiv").show(); });
		
		refresh();
		
		$("ul.nav-tabs li").click(function() {
			if($(this).hasClass("active"))  return;
			
			refresh($(this).attr("type"));
		});
		
		function refresh(type) {
			var t = type ? type : parseInt($("li.active").attr("type")), $tab = $("table.list-table");
			
			_remoteCall("user/getUsers.do", {type: t}, function(data) {
				var info = eval(data), html = "";
				
				// remove origin rows
				$("tr:gt(0)", $tab).remove();
				
				for(var i in info) {
					html += "<tr id='" + info[i].id + "'>";
					html += "<td class='sn'>" + (parseInt(i) + 1) + "</td>";
					html += "<td><div>" + info[i].username + "</div></td>";
					html += "<td><div>" + info[i].email + "</div></td>";
					html += "<td><div>" + info[i].address + "</div></td>";
					
					html += "<td><div class='hide'>";
					html += "<i class='icon-pencil' style='cursor: pointer;margin-right: 10px;' onclick='editUser($(this));'></i>";
					html += "<i class='icon-trash' style='cursor: pointer;' onclick='removeUser($(this));'></i>";
					html += "</div></td>";
					
					html += "</tr>";
				}
				
				$tab.append(html);
				
				// bind mouse event
				if(isAdmin) {
					$("tr:gt(0)", $tab).hover(
						function() {
							$("td:last div", this).show();
						}, 
						function() {
							$("td:last div", this).hide();
						}
					);
				}
			});
		}
	
		function saveUser() {
			if(!checkForm())  return;
			
			var p1 = $("#username").val();
			var p2 = $("#password").val();
			var p3 = $("#email").val();
			var p4 = $("#address").val();
			
			var t = parseInt($("li.active").attr("type"));
				
			_remoteCall("user/saveUser.do", {id: globalUserId, type: t, username: p1, password: p2, email: p3, address: p4}, function(data) {
				refresh();
				$("#userModal").modal("hide");
			});
		}
		
		function removeUser($i) {
			$("#dialog-confirm").dialog({
				resizable: false,
			    height: 200,
			    modal: true,
			    buttons: {
			        "確定": function() {
			        	_remoteCall("user/removeUser.do", {userId: $i.closest("tr").attr("id")}, function() {
							$i.closest("tr").remove();
							
							$("#dialog-del").dialog({
								resizable: false,
							    height: 200,
							    modal: true,
							    buttons: {
							        "確定": function() {
							        	$(this).dialog("close");
							        	$("#dialog-confirm").dialog("close");
							        }
								}
							});
						});
			        },
			        "取消": function() {
			            $(this).dialog("close");
			    	}
				}
			});
		}
		
		function createUser() {
			globalUserId = "";
			
			$("#myModalLabel").text("添加用户");
			$("#username, #password, #email, #address").val("");
			$("#userModal").modal("show");
		}
		
		function editUser($i) {
			var $tr = $i.closest("tr");
			globalUserId = $tr.attr("id");
			
			_remoteCall("user/getPassword.do", {userId: globalUserId}, function(data) {
				
				$("#password").val(data);
				
				$("#myModalLabel").text("编辑用户");
				$("#username").val($("td:eq(1)", $tr).text());
				$("#email").val($("td:eq(2)", $tr).text());
				$("#address").val($("td:eq(3)", $tr).text());
				
				$("#userModal").modal("show");
			});
		}
	</script>
	
</body>
</html>
