<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cloud" tagdir="/WEB-INF/tags/" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<style>
		.input-text {width: 460px;}
		textarea {width: 460px;height: 150px;}
	</style>
</head>

<body>
	<div class="wrapper">
	
		<form action="<c:url value="/project/saveProject.do" />" onsubmit="return initForm();" method="post">
		
		<div id="btnDiv">
			<input type="submit" class="btn" value="保存" />
			<a href="<c:url value="/pages/project/project.jsp" />" class="btn">返回</a>
		</div>
		
		<table class="edit-table">
			<tr>
				<td class="left-td" width="80px">项目名称</td>
				<td><input name="name" type="text" class="input-text input-require" value="${project.name}" /></td>
			</tr>
			<tr>
				<td class="left-td">项目经理</td>
				<td><input id="manager" type="text" class="input-text input-require" ondblclick="showUsers($(this));" onkeydown="return false;" val="${project.managerId}" value="${project.manager}" /><i onclick="$(this).prev().dblclick();" class="icon-search input-icon"></i></td>
			</tr>
			<tr>
				<td class="left-td">起始日期</td>
				<td><input id="startDate" name="start" type="text" class="input-text" onkeydown="return false;" value="<cloud:date date="${project.startDate}" />" /><i onclick="$(this).prev().focus();" class="icon-search input-icon"></i></td>
			</tr>
			<tr>
				<td class="left-td">结束日期</td>
				<td><input id="endDate" name="end" type="text" class="input-text" onkeydown="return false;" value="<cloud:date date="${project.endDate}" />" /><i onclick="$(this).prev().focus();" class="icon-search input-icon"></i></td>
			</tr>
			<tr>
				<td class="left-td">项目描述</td>
				<td><textarea name="intro">${project.intro}</textarea></td>
			</tr>
		</table>
		
		<input type="hidden" name="id" value="${project.id}" />
		<input type="hidden" id="_managerId" name="managerId" />
		
		</form>
	</div>

	<jsp:include page="/pages/common/userSelect.jsp"></jsp:include>
	
	<script>
		$("#startDate, #endDate").datepicker({
			dateFormat: "yy-mm-dd"
		});
		
		function initForm() {
			var val = $("#manager").attr("val");
			$("#_managerId").val(val ? val : "");
			
			return checkForm();
		}
	</script>
</body>
</html>
