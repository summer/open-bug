package com.cloud.platform;

public class HqlUtil {

	/**
	 * combine hql in string
	 * 
	 * @param strs
	 * @return
	 */
	public static String combineInHql(String strs, boolean isString) {
		
		if(StringUtil.isNullOrEmpty(strs)) {
			return "";
		}
		
		String[] arr = strs.split(",");
		StringBuffer sb = new StringBuffer();
		String prefix = " in (", endfix = ")";
		
		for(String s : arr) {
			
			if(sb.length() == 0) {
				sb.append(prefix);
			} else {
				sb.append(",");
			}
			
			if(isString) {
				sb.append("'" + s + "'");
			} else {
				sb.append(s);
			}
		}
		
		sb.append(endfix);
		
		return sb.toString();
	}
}
