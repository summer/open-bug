package com.cloud.bug.util;

import java.util.ArrayList;
import java.util.List;

public class BaseInfoUtil {
	
	public static String ITEM_LEVEL_1 = "致命";
	public static String ITEM_LEVEL_2 = "严重";
	public static String ITEM_LEVEL_3 = "一般";
	public static String ITEM_LEVEL_4 = "优化";
	
	public static String ITEM_PRIORITY_1 = "高";
	public static String ITEM_PRIORITY_2 = "中";
	public static String ITEM_PRIORITY_3 = "低";

	/**
	 * get field base items
	 * 
	 * @param field
	 * @return
	 */
	public static List<Object[]> getBaseItems(String field) {
		
		List<Object[]> items = new ArrayList();
		
		// level field
		if("level".equals(field)) {
			items.add(new Object[] {1, ITEM_LEVEL_1});
			items.add(new Object[] {2, ITEM_LEVEL_2});
			items.add(new Object[] {3, ITEM_LEVEL_3});
			items.add(new Object[] {4, ITEM_LEVEL_4});
		}
		// priority field
		else if("priority".equals(field)) {
			items.add(new Object[] {1, ITEM_PRIORITY_1});
			items.add(new Object[] {2, ITEM_PRIORITY_2});
			items.add(new Object[] {3, ITEM_PRIORITY_3});
		}
		
		return items;
	}
}
